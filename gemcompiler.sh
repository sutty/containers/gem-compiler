#!/bin/sh

export PATH=usr/lib/ccache/bin:$PATH
PLATFORM=`ruby -e "puts RbConfig::CONFIG['arch']"`

cd /srv/gems

test -f ${GEM}-${VERSION}.gem \
  || gem fetch --source=https://${SUTTY} --platform=ruby \
               --version ${VERSION} ${GEM}

test -f ${GEM}-${VERSION}-${PLATFORM}.gem \
  || gem compile -V --prune --strip "strip --strip-unneeded" ${GEM}-${VERSION}.gem

gem inabox -o ${GEM}-${VERSION}-${PLATFORM}.gem \
  --host https://${HTTP_BASIC_USER}:${HTTP_BASIC_PASSWORD}@gems.${SUTTY}
