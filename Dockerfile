FROM sutty/sdk-ruby:latest
MAINTAINER "f <f@sutty.nl>"

ENV GEM=none
ENV VERSION=0
ENV SUTTY=sutty.nl
ENV HTTP_BASIC_USER=sutty
ENV HTTP_BASIC_PASSWORD=gibberish
ENV NOKOGIRI_USE_SYSTEM_LIBRARIES=1

RUN gem install --no-user-install --no-document --source https://rubygems.org geminabox mini_portile2 gem-compiler

COPY ./gemcompiler.sh /usr/local/bin/gemcompiler
RUN chmod 755 /usr/local/bin/gemcompiler

RUN install -dm 2750 -o builder -g builder /srv/gems
VOLUME /srv/gems

RUN apk add --no-cache file-dev
RUN apk add --no-cache libssh2-dev
RUN apk add --no-cache libc6-compat libstdc++

USER builder
ENTRYPOINT /usr/local/bin/gemcompiler
